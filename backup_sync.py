__author__ = 'Administrator'

import os
import shutil


def sync(sd, td):
    print('sync dir: ', sd)
    if not os.path.exists(td):
        print('copy dir: ', sd)
        shutil.copytree(sd, td)
        return 0

    slist = os.listdir(sd)
    tlist = os.listdir(td)
    for name in slist:
        spath = sd + '/' + name
        tpath = td + '/' + name
        if name not in tlist:
            if os.path.isfile(spath):
                print('copy file: ', spath)
                shutil.copy2(spath, tpath)
            else:
                print('copy dir: ', spath)
                shutil.copytree(spath, tpath)
        else:
            tlist.remove(name)

            if os.path.isfile(spath) and os.path.isfile(tpath):
                deal_with_file(spath, tpath)
            elif not os.path.isfile(spath) and not os.path.isfile(tpath):
                sync(spath, tpath)  # deal with directory
            else: #same name but one is a file and another is a directory
                if os.path.isfile(tpath):
                    print('remove file: ', tpath)
                    os.remove(tpath)
                    print('copy dir: ', spath)
                    shutil.copytree(spath, tpath)
                else:
                    print('remove dir: ', tpath)
                    shutil.rmtree(tpath)
                    print('copy file: ', spath)
                    shutil.copy2(spath, tpath)

    for tname in tlist:
        tpath = td + '/' + tname
        if os.path.isfile(tpath):
            print('remove file: ', tpath)
            os.remove(tpath)
        else:
            print('remove dir: ', tpath)
            shutil.rmtree(tpath)


def deal_with_file(spath, tpath):
    '''compare files' modified time and size, if not the same, overwrite it'''
    if os.path.getmtime(spath) != os.path.getmtime(tpath) \
            or os.path.getsize(spath) != os.path.getsize(tpath):
        print('overwrite file: ', tpath)
        shutil.copy2(spath, tpath)



if __name__ == '__main__':
    sd = 'E:/temp/sd'
    td = 'E:/temp/td'
    sync(sd, td)